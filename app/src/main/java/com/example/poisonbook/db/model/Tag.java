package com.example.poisonbook.db.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;

@Entity(tableName = "tagsTable",
        foreignKeys = @ForeignKey(entity = Poison.class,
                parentColumns = "forTag",
                childColumns = "tagId"),
        primaryKeys = {"tagId", "tag"})

public class Tag {
    @NonNull
    int tagId;
    @NonNull
    String tag;

    public Tag(@NonNull int tagId, @NonNull String tag) {
        this.tagId = tagId;
        this.tag = tag;
    }

    @NonNull
    public int getTagId() {
        return tagId;
    }

    public void setTagId(@NonNull int tagId) {
        this.tagId = tagId;
    }

    @NonNull
    public String getTag() {
        return tag;
    }

    public void setTag(@NonNull String tag) {
        this.tag = tag;
    }
}

