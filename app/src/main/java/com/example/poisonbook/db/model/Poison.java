package com.example.poisonbook.db.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity
public class Poison {
    @NonNull
    @PrimaryKey (autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    @NonNull
    private String title;
    private String path;
    private String tags;
    @ColumnInfo(name = "forTag")
    int forTag;


    public Poison(String title, String path, String tags) {
        this.title = title;
        this.path = path;
        this.tags = tags;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    public void setTitle(@NonNull String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public int getForTag() {
        return forTag;
    }

    public void setForTag(int forTag) {
        this.forTag = forTag;
    }
    
}
