package com.example.poisonbook.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.poisonbook.db.model.Poison;

import java.util.List;

@Dao
public interface DataDao {
    @Insert
    void insert(Poison poison);

    @Delete
    void delete(Poison poison);

    @Query("SELECT * FROM Poison")
    List<Poison> getAllData();

    @Query("SELECT * FROM Poison WHERE id = :id")
    Poison getById(long id);

    //пример запроса с выборкой
    @Query("SELECT * FROM Poison WHERE title LIKE :title")
    List<Poison> getByTitle(String title);

    @Update
    void update(Poison poison);

    @Query("SELECT * FROM Poison ORDER BY title")
    List <Poison> sortByTitle();

    @Query("SELECT * FROM Poison WHERE tags LIKE :tags")
    List<Poison> selectByTags(String tags);
}