package com.example.poisonbook;

import android.app.Application;

import androidx.room.Room;

import com.example.poisonbook.db.DatabaseHelper;
import com.example.poisonbook.db.model.Poison;


public class App extends Application {

    private static App instance;
    private DatabaseHelper db;

    String[] tags = new String[]{"Психотропные ", "Нервно-паралитические ", "Удушающие ",
            "Вызывающие слезотечение и раздражающие слизистые покровы ",
            "Кожно-резобтивные ", "Животного происхождения ", "Растительного происхождения ",
            "Химические вещества ", "Радиоактивные "};

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        db = Room.databaseBuilder(getApplicationContext(), DatabaseHelper.class, "jddrydtkkffjkg")
                .allowMainThreadQueries()
                .build();
        //fillDB();
    }//4276480012883222

    private void fillDB() {
        db.getDataDao().insert(new Poison("Болиголов", "boligolov", tags[1] + tags[2] + tags[6]));
        db.getDataDao().insert(new Poison("Беладонна (Красавка)", "beladonna", tags[1] + tags[4] + tags[6]));
        db.getDataDao().insert(new Poison("Борец (Аконит)", "akonit", tags[2] + tags[6]));
        db.getDataDao().insert(new Poison("Диметилртуть", "c2h6hg", tags[1] + tags[7]));
        db.getDataDao().insert(new Poison("Тетродотоксин", "tetrodotox", tags[1] + tags[5]));
        db.getDataDao().insert(new Poison("Полоний", "polonium", tags[7] + tags[8]));
        db.getDataDao().insert(new Poison("Ртуть", "hg", tags[1] + tags[7]));
        db.getDataDao().insert(new Poison("Цианид", "cianid", tags[2] + tags[7]));
        db.getDataDao().insert(new Poison("Ботулотоксин", "botulotox", tags[1] + tags[3]));
        db.getDataDao().insert(new Poison("Мышьяк", "as", tags[4] + tags[7]));
    }

    public DatabaseHelper getDatabaseInstance() {
        return db;
    }
}