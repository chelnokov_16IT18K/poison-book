package com.example.poisonbook.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.poisonbook.App;
import com.example.poisonbook.R;
import com.example.poisonbook.db.DatabaseHelper;
import com.example.poisonbook.db.model.Poison;
import com.example.poisonbook.ui.adapter.PoisonDataAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements PoisonDataAdapter.OnDeleteListener, PoisonDataAdapter.AdapterCallback, CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private DatabaseHelper databaseHelper;
    PoisonDataAdapter recyclerAdapter;

    int id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        databaseHelper = App.getInstance().getDatabaseInstance();

        recyclerAdapter = new PoisonDataAdapter(this, databaseHelper.getDataDao().getAllData());

        Switch sortSwitch = findViewById(R.id.switch_sort);
        if (sortSwitch != null) {
            sortSwitch.setOnCheckedChangeListener(this);
        }

        Switch selectSwitch = findViewById(R.id.switch_select);
        if (selectSwitch != null) {
            selectSwitch.setOnCheckedChangeListener(this);
        }
        recyclerView.setAdapter(recyclerAdapter);

        getCounterView();


        recyclerAdapter.setOnItemClickListener((itemView, position, view) -> {
            Intent intent = new Intent(getApplicationContext(), ReadActivity.class);
            intent.putExtra("id", id);
            startActivity(intent);
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_button, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add) {
            startActivity(new Intent(this, AddDataActivity.class));
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Switch myswitch = findViewById(R.id.switch_sort);
        if (myswitch != null) {
            myswitch.setOnCheckedChangeListener(this);
        }
        PoisonDataAdapter recyclerAdapter = new PoisonDataAdapter(this, databaseHelper.getDataDao().getAllData());
        recyclerAdapter.setOnDeleteListener(this);
        recyclerView.setAdapter(recyclerAdapter);
    }

    @Override
    public void onDelete(Poison poison) {
        databaseHelper.getDataDao().delete(poison);
        recyclerAdapter.dataSetChanged(databaseHelper.getDataDao().getAllData());
        getCounterView();
    }

    private void getCounterView() {
        TextView counterView = findViewById(R.id.counterView);
        counterView.setText("Найдено элементов: " + recyclerAdapter.getItemCount());
    }

    @Override
    public void onMethodCallback(int id) {
        this.id = id;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        PoisonDataAdapter recyclerAdapter = new PoisonDataAdapter(this, databaseHelper.getDataDao().getAllData());
        switch (buttonView.getId()) {
            case R.id.switch_sort:
                Toast.makeText(this, "Сортировка по имени: " + (isChecked ? "Включена" : "Выключена"),
                        Toast.LENGTH_SHORT).show();
                if (isChecked) {
                    recyclerAdapter.dataSetChanged(databaseHelper.getDataDao().sortByTitle());
                } else {
                    recyclerAdapter.dataSetChanged(databaseHelper.getDataDao().getAllData());
                }

                break;
            case R.id.switch_select:
                Toast.makeText(this, "Выборка: " + (isChecked ? "Включена" : "Выключена"),
                        Toast.LENGTH_SHORT).show();
                if (isChecked) {
                    recyclerAdapter.dataSetChanged(databaseHelper.getDataDao().selectByTags("%Растительного происхождения%"));
                } else {
                    recyclerAdapter.dataSetChanged(databaseHelper.getDataDao().getAllData());
                }
                break;
        }

        recyclerAdapter.setOnDeleteListener(this);
        recyclerView.setAdapter(recyclerAdapter);
    }
}