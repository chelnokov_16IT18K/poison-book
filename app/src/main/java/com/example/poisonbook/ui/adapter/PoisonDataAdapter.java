package com.example.poisonbook.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.poisonbook.R;
import com.example.poisonbook.db.model.Poison;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PoisonDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<Poison> poisonList = new ArrayList<>();
    private OnDeleteListener onDeleteListener;
    private Context context;

    private AdapterCallback mAdapterCallback;

    public PoisonDataAdapter(Context context, List<Poison> poisonList) {
        this.context = context;
        this.poisonList = poisonList;
        try {
            this.mAdapterCallback = ((AdapterCallback) context);
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AdapterCallback.");
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_some_data, parent, false);
        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final NewsViewHolder viewHolder = (NewsViewHolder) holder;
        viewHolder.title.setText(poisonList.get(position).getTitle());
        viewHolder.description.setText(poisonList.get(position).getTags().toString());
    }

    @Override
    public int getItemCount() {
        return poisonList.size();
    }

    private static OnItemClickListener listener;

    public interface OnItemClickListener{
        void onItemClick(View itemView, int position, View view);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        PoisonDataAdapter.listener = listener;
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        public TextView title;
        @BindView(R.id.path)
        public TextView description;
        public ImageView img;
        @BindView(R.id.delete)
        public TextView delete;

        public NewsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> {
                if (PoisonDataAdapter.listener!=null){
                    mAdapterCallback.onMethodCallback(poisonList.get(getLayoutPosition()).getId());
                    PoisonDataAdapter.listener.onItemClick(itemView, getLayoutPosition(), v);
                }
            });
            delete.setOnClickListener(view -> {
                onDeleteListener.onDelete(poisonList.get(getAdapterPosition()));
                poisonList.remove(getAdapterPosition());
                notifyItemRemoved(getAdapterPosition());
            });
            img = itemView.findViewById(R.id.img);
        }
    }

    public void dataSetChanged (List <Poison> poisonList){
        this.poisonList = poisonList;
        notifyDataSetChanged();
    }

    public void setOnDeleteListener(OnDeleteListener onDeleteListener) {
        this.onDeleteListener = onDeleteListener;
    }

    public interface OnDeleteListener {
        void onDelete(Poison poison);
    }


    public interface AdapterCallback {
        void onMethodCallback(int id);
    }
}