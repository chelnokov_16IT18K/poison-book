package com.example.poisonbook.ui;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.poisonbook.App;
import com.example.poisonbook.R;
import com.example.poisonbook.db.DatabaseHelper;
import com.example.poisonbook.db.model.Poison;
import com.example.poisonbook.ui.adapter.PoisonDataAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddDataActivity extends AppCompatActivity {

    @BindView(R.id.title)
    EditText titleEdit;
    @BindView(R.id.path)
    EditText tagEdit;

    DatabaseHelper databaseHelper;
    PoisonDataAdapter dataAdapter;
    int id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        ButterKnife.bind(this);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        databaseHelper = App.getInstance().getDatabaseInstance();

    }

    @OnClick(R.id.save)
    public void onSaveClick() {
        DatabaseHelper databaseHelper = App.getInstance().getDatabaseInstance();
        Poison poison = new Poison(titleEdit.getText().toString(), "path",tagEdit.getText().toString());
        databaseHelper.getDataDao().insert(poison);
        dataAdapter.dataSetChanged(databaseHelper.getDataDao().getAllData());
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

}