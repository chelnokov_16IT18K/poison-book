package com.example.poisonbook.ui;


import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.poisonbook.App;
import com.example.poisonbook.R;
import com.example.poisonbook.db.DatabaseHelper;
import com.example.poisonbook.db.model.Poison;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReadActivity extends AppCompatActivity {

    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        ImageView imageView = findViewById(R.id.imageView);
        imageView.setImageDrawable(getDrawable(R.drawable.skull));

        databaseHelper = App.getInstance().getDatabaseInstance();

        int id = getIntent().getIntExtra("id", 0);
        Poison poison =databaseHelper.getDataDao().getById(id);
        viewFile(poison.getPath());


        ImageView imageView2 = findViewById(R.id.imageView2);
        imageView2.setImageDrawable(getDrawable(R.drawable.skull));
    }


    private void viewFile(String fileName) {
        StringBuilder stringBuilder = getStringFromAssetFile(fileName);
        String content = stringBuilder.toString();
        TextView textView = findViewById(R.id.textView);
        textView.setText(Html.fromHtml(content));
    }


    protected StringBuilder getStringFromAssetFile(String fileName) {
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(getAssets().open(fileName)))) {
            String string;
            while ((string = reader.readLine()) != null) {
                stringBuilder.append(string);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

